# README #


### What is this repository for? ###

* This is application used for Atlassian assignment
* This includes 3 test cases:
*  Case1 : log in to system.
*  Case2: log in to system then create one issue.
*  Case3: log in to system then search issue and edit that issue.
* Version 1.0
* Created by Tan Nguyen

### How do I get set up? ###

* Import selenium 2.45
* Copy IE and Chrome driver then change the path in MainTest threat