package MyPackage;



import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;


public class MainTest {
	
	//Set property
	@BeforeClass
	public static void initdriver()
	{
		System.setProperty("webdriver.chrome.driver", "d://chromedriver.exe");
	}
	//Click and navigate to Login page
	WebDriver driver = new ChromeDriver();
	
	/*Using IE Driver
	   File file = new File("D://IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
	  */
	/*
	  Using Firefox Driver
	  WebDriver driver = new FirefoxDriver;
	 */
	//Before running cases, maximize window and load URL
	@Before
	public void before()
	{
		driver.manage().window().maximize();
		final String URL = "https://jira.atlassian.com/browse/TST";
		driver.get(URL);
	}
	//Log in test case
	@Test
	public void login() {
		DashboardPage dashboardPage = PageFactory.initElements(driver, DashboardPage.class);
		LoginPage loginPage = dashboardPage.toLoginPage();	
		loginPage.loginAs("tantest13990@gmail.com", "13091990");
	}
		
	//Create test case
	@Test
	public void createtestcase() throws InterruptedException {
		DashboardPage dashboardPage = PageFactory.initElements(driver, DashboardPage.class);
		LoginPage loginPage = dashboardPage.toLoginPage();	
		loginPage.loginAs("tantest13990@gmail.com", "13091990");
		CreateIssuePage createissue =dashboardPage.tocreateIssuepage();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		createissue.backtodashboard("This is test defect created by Tan Nguyen");
	
	}
	//Search and Edit test case
	@Test
	public void searchandedittestcase() throws InterruptedException {
		DashboardPage dashboardPage = PageFactory.initElements(driver, DashboardPage.class);
		LoginPage loginPage = dashboardPage.toLoginPage();	
		loginPage.loginAs("tantest13990@gmail.com", "13091990");
		IssuePage issuepage = dashboardPage.toIssuepage();
		issuepage.searchforissue("project = \"A Test Project\" AND reporter = currentUser() and id =\"TST-63949\"");
		issuepage.editissue("This issues has been edited by selenium");
		Thread.sleep(3000);
		dashboardPage.logout();
	
	}
	//After test cases were run, quit driver
	@After
	public void after()
	{
		driver.quit();
	}

}
