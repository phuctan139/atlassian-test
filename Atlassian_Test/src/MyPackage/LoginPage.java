package MyPackage;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;


public class LoginPage {
	// Create a new instance of a driver
	private final WebDriver driver;
	//find elements on page
	@FindBy(how = How.ID, using = "username") private WebElement user;
	@FindBy(how = How.ID, using = "password") private WebElement password;
	@FindBy(how = How.ID, using = "login-submit") private WebElement loginbutton;
	
	//Contructor
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(10, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);

		wait.until(ExpectedConditions.titleIs("Sign in to continue"));
	        
	}
	//Login as User and back to dashboar page
	public DashboardPage loginAs(String username, String password) {

		executeLogin(username, password);
		return PageFactory.initElements(driver, DashboardPage.class);
		
	}
	//execute login
	private void executeLogin(String username, String password) {

		user.sendKeys(username);
		this.password.sendKeys(password);
		loginbutton.click();
	}

	
}
