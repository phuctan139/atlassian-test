package MyPackage;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;



public class DashboardPage {
		
	//find elements on page
	@FindBy(how = How.ID, using = "user-options") private WebElement btnlogin;
	@FindBy(how = How.ID, using = "create_link") private WebElement createissue;
	@FindBy(how = How.ID, using = "issuetype-single-select") private WebElement selectbugtypebutton;
	@FindBy(how = How.ID, using = "issuetype-suggestions") private WebElement selectBug;
	@FindBy(how = How.ID, using = "summary") private WebElement summary;
	@FindBy(how = How.ID, using = "create-issue-submit") private WebElement createbutton;
	@FindBy(how = How.ID, using = "find_link") private WebElement issuemenu;
	@FindBy(how = How.ID, using = "issues_new_search_link_lnk") private WebElement searchforissue;
	@FindBy(how = How.ID, using = "header-details-user-fullname") private WebElement selectprofile;
	@FindBy(how = How.ID, using = "log_out") private WebElement btnlogout;
	
	// Create a new instance of a driver
	private final WebDriver driver;
	//Contructor 
	public DashboardPage(WebDriver driver) {
		super();
		this.driver = driver;	
        //Verify page base on page's title
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(10, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);
		wait.until(ExpectedConditions.titleIs("CBang - Agile Board - Atlassian JIRA"));

	}
	
	//Click and navigate to Login page
	public LoginPage toLoginPage()
	{
		btnlogin.click();
		return PageFactory.initElements(driver, LoginPage.class);
		
	}
	
	//Click and navigate to Create issue page
	public CreateIssuePage tocreateIssuepage()
	{
		createissue.click();
		return PageFactory.initElements(driver, CreateIssuePage.class);
	}
	
	//method wait to element is clickable
	public void waittoclickable(WebElement e)
	{
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(30, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);
		wait.until(ExpectedConditions.elementToBeClickable(e));
	}
	
	//Click and navigate to Issue page
	public IssuePage toIssuepage()
	{
		issuemenu.click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		searchforissue.click();
		return PageFactory.initElements(driver, IssuePage.class);
	}
	
	//logout
	public void logout()
	{
		waittoclickable(selectprofile);
		selectprofile.click();
		btnlogout.click();
	}
}
