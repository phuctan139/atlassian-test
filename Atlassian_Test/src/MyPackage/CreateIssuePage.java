package MyPackage;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;



public class CreateIssuePage {
	//find elements on page
	@FindBy(how = How.ID, using = "issuetype-single-select") private WebElement selectbugtypebutton;
	@FindBy(how = How.ID, using = "issuetype-suggestions") private WebElement selectBug;
	@FindBy(how = How.ID, using = "summary") private WebElement summary;
	@FindBy(how = How.ID, using = "create-issue-submit") private WebElement createbutton;
	@FindBy(how = How.ID, using = "find_link") private WebElement issuemenu;
	@FindBy(how = How.ID, using = "issues_new_search_link_lnk") private WebElement searchforissue;
	// Create a new instance of a driver
	private final WebDriver driver;
	//Contructor 
	public CreateIssuePage(WebDriver driver) {
		super();
		this.driver = driver;

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(10, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);

		wait.until(ExpectedConditions.titleIs("Create Issue - Atlassian JIRA"));

	}
	//wait element is displayed
	public void waittodisplay(WebElement e)
	{
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(30, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);
		wait.until(ExpectedConditions.visibilityOf(e));
	}
	//create one issue 
	public void createIssue (String summany) throws InterruptedException 
	{		
		selectbugtypebutton.click();
		selectBug.click();
		Thread.sleep(3000);;
		summary.sendKeys(summany);
		createbutton.click();
			
	}
	//back to dashbboard page after creating issue successfully
	public DashboardPage backtodashboard(String summary) throws InterruptedException 
	{
		createIssue(summary);
		return PageFactory.initElements(driver, DashboardPage.class);
	}
}
