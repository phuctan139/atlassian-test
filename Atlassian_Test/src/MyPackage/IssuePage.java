package MyPackage;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;


public class IssuePage {
	//find elements on page
	@FindBy(how = How.ID, using = "advanced-search") private WebElement txtadvancedsearch;
	@FindBy(how = How.LINK_TEXT, using = "Advanced") private WebElement btnadvance;
	@FindBy(how = How.XPATH, using = "//button[@type='button']") private WebElement btnsearch;
	@FindBy(how = How.XPATH, using = "//section[@id='content']/div/div[4]/div/div/div/div/div/div/div/div[2]/ol/li/a/span") private WebElement selectissue;
	@FindBy(how = How.XPATH, using = ".//*[@id='comment-issue']") private WebElement btncomment;
	@FindBy(how = How.XPATH, using = "//div/div/div/div/div/ul/li/a/span") private WebElement btnedit;
	@FindBy(how = How.ID, using = "summary") private WebElement txtsummary;
	@FindBy(how = How.ID, using = "edit-issue-submit") private WebElement btneditsubmit;
	@FindBy(how = How.ID, using = "aui-flag-container") private WebElement message;
	
	// Create a new instance of a driver
	private final WebDriver driver;
	
	//Contructor
	public IssuePage(WebDriver driver) {
		super();
		this.driver = driver;

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(30, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);

		wait.until(ExpectedConditions.titleIs("Issue Navigator - Atlassian JIRA"));

	}
	//Check if element is present
	protected boolean isElementPresent(By by){
        try{
            driver.findElement(by);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
	//Search for issue by using JQL
	public void searchforissue(String querry) 
	{
		if(isElementPresent(By.id("advanced-search")))
		{
			txtadvancedsearch.clear();
			txtadvancedsearch.sendKeys(querry);
			btnsearch.click();		
		}
		else
		{
			btnadvance.click();
			txtadvancedsearch.clear();
			txtadvancedsearch.sendKeys(querry);
			btnsearch.click();
			
		}
			
	}
	//wait element is displayed
	public void waittodisplay(WebElement we)
	{
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
	            .withTimeout(30, TimeUnit.SECONDS)
	            .pollingEvery(2, TimeUnit.MILLISECONDS)
	            .ignoring(NoSuchElementException.class);
		wait.until(ExpectedConditions.visibilityOf(we));
	}
	//Edit SUMMARY of issue
	public void editissue(String string) throws InterruptedException 
	{
	
		waittodisplay(btnedit);
		btnedit.click();
		Thread.sleep(3000);
		txtsummary.sendKeys(string);
		btneditsubmit.click();
	}


	
}
